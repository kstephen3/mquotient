var express = require('express')
,MongoStore=require('connect-mongo')(express)
,expressSession = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');
var surveyModels=require('./models/surveyModel');
var survey=require('./routes/survey');
var apiSurvey=require('./api/survey');
var dbConnection=require('./db/dbConnection');
var mongoose=require('mongoose');

// start a connection to the db
dbConnection.startConnection();
var app = express();
var ip =process.env.OPENSHIFT_NODEJS_IP||'127.0.0.1' //ipAndPort.ipaddress;
var port = process.env.OPENSHIFT_NODEJS_PORT||'3001'//ipAndPort.port;
app.listen(process.env.port||port,ip,function(){
  console.log("%s: Node  Server started at %s on port: %d",ip ,Date(),port);
})
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
/*
if(process.env.OPENSHIFT_MONGODB_DB_URL==="undefined"){
    app.use(expressSession({
        saveUninitialized:false//dont create the session unless something is stored
        ,secret:"kazibwe"
        ,resave:false//dont resave the session unless something is modified
        ,store:new MongoStore({
            mongooseConnection:db.mongoose.connection
            ,db:"mysurveysessiondb"})//"smssessionsdb"//"SmsSessionsDb"})
        ,autoRemove:true
        ,autoRemoveInterval:10//in minutes
        //,ttl: 14 * 24 * 60 * 60 // = 14 days. Default
        // ,touchAfter:24*3600//time period in seconds.update the session only once in 24hrs,except for those that change
        // something on session data,no matter how many requests are made.
    }));

}else{
    app.use(expressSession({
        saveUninitialized:false//dont create the session unless something is stored
        ,secret:"kazibwe"
        ,resave:false//dont resave the session unless something is modified
        ,store:new MongoStore({url: process.env.OPENSHIFT_MONGODB_DB_URL+ process.env.OPENSHIFT_APP_NAME,
            db:""+process.env.OPENSHIFT_APP_NAME+"" })
        ,autoRemove:true
        ,autoRemoveInterval:10//in minutes

    }));
}
*/

/*
app.use(expressSession({
    saveUninitialized:false //dont create the session unless something is stored
    ,resave:false //dont resave the session unless something is modified
    ,secret: 'kazibwe'
    ,store:new MongoStore({
        mongooseConnection:mongoose.connection,
        db:'mysurveysessiondb'
    })
    ,autoRermove:true
    ,autoRemoveInterval:10 //in minutes
}));*/
app.use(function (req, res, next) {
  res.locals.isAuthenticated=false;
  console.log("-------------------configuring------------------")
  next();
});

app.get('/',routes.index)
/*app.all('/!*',function(req,res,next){
  if(req.session.user&&(req.session.isAuthenticated==true)){
    res.locals.isAuthenticated=true;

  }else{
    res.locals.isAuthenticated=false;
  }
  next()
})*/
app.get('/survey/list',survey.surveyList);
app.get('/api/survey/list',apiSurvey.surveyList);
app.get('/survey/create',survey.createSurvey);
app.post('/survey/create',survey.doCreateSurvey);
app.get('/survey/delete/:surveyId',survey.deleteSurvey);
app.get('/survey/questions/:surveyId',survey.questionListBySurveyId);
app.get('/api/survey/questions/:surveyId',apiSurvey.questionListBySurveyId);
app.post('/api/survey/create/question/:surveyId',apiSurvey.doCreateQuestion);
app.delete('/api/survey/delete/question/:questionId',apiSurvey.doDeleteQuestion);
app.get('/api/user/code/:phoneNumber',apiSurvey.getUserCode);
app.post('/api/survey/create/user/codes/:surveyId',apiSurvey.doCreateCompanyUserCodes);

//app.get('/survey/create/question',survey.createQuestion);
app.get('/survey/create/question/:surveyId',survey.createQuestion);
app.post('/survey/create/question',survey.doCreateQuestion);
app.get('/question/list',survey.questionList);
app.post('/api/login', apiSurvey.login);
app.get('/api/logout', apiSurvey.doLogOut);
app.post('/api/survey/register',apiSurvey.doRegisterUser);
app.get('/api/authority', apiSurvey.getAuth);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});



module.exports = app;
