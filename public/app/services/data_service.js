/**
 * Created by sempa on 1/4/2016.
 */
'use strict'
var dataService=surveyApp.factory('dataService',['$http','$q',function($http,$q){
    var service={onBeforeAjax:null,onAfterAjax:null};
    service.$http=$http;
    service.url=function(link){
        return  link;
    }
    service.navigate=function(link){
        window.location.replace(service.url(link));
    }
    service.getReff=function(id,version){
        if(!service.isEmpty(id)){
            return{id:id,version:version};
        }
    }
    service.isEmpty=function(val){
        return undefined===val||null===val||""===val;
    }
    service.getDirectPromise=function(data){
        var deferred=$q.defer();
        deferred.resolve(data);
        return deferred.promise;
    }
    service.post=function(method,input,$scope){
        service.ajaxStart();
        var deferred=$q.defer();
        service.$http({
            method:'POST',
            url:service.url(method),
            data:input
        }).success(function(data,status,headers,config){
            service.clearErrors($scope);
            deferred.resolve(data);
            service.ajaxEnd();
        }).error(function(data,status,headers,config){
            deferred.reject(data);
            service.ajaxEnd();
            service.showErrors($scope,data);
        });
        return deferred.promise;
    }
    service.get=function(method,input,$scope){
        service.ajaxStart();
        var deferred=$q.defer();
        service.$http({
            method:'GET',
            url:service.url(method)
        }).success(function(data,status,headers,config){
           // console.log('-----------------------data in service-----------------------');
           // console.log(data);
            //console.log('-----------------------status in service-----------------------');
           // console.log(status);
            //console.log('------------------config in service-----------------------');
           // console.log(config);
            service.clearErrors($scope);
            deferred.resolve(data);
            service.ajaxEnd();
        }).error(function(data,status,headers,config){
            deferred.reject(data);
            service.ajaxEnd();
            service.showErrors($scope,data);
        });
        return deferred.promise;
    }
    service.delete=function(method,input,$scope){
        service.ajaxStart();
        var deferred=$q.defer();
        service.$http({
            method:'DELETE',
            url:service.url(method),
            data:input
        }).success(function(data,status,headers,config){
            service.clearErrors($scope);
            deferred.resolve(data);
            service.ajaxEnd();
        }).error(function(data,status,headers,config){
            deferred.reject(data);
            service.ajaxEnd();
            service.showErrors($scope,data);
        });
        return deferred.promise;
    }
    service.put=function(method,input,$scope){
        service.ajaxStart();
        var deferred=$q.defer();
        service.$http({
            method:'POST',
            url:service.url(method),
            data:input
        }).success(function(data,status,headers,config){
            service.clearErrors($scope);
            deferred.resolve(data);
            service.ajaxEnd();
        }).error(function(data,status,headers,config){
            deferred.reject();
            service.ajaxEnd();
            service.showErrors($scope,data);
        });
        return deferred.promise;
    }

    service.all=function(promises){
        return $q.all(promises);
    }
    service.ajaxCallCount=0;
    service.ajaxStart=function(){
        if(0===service.ajaxCallCount){
            if(service.onBeforeAjax) service.onBeforeAjax();
        }
        service.ajaxCallCount++;
    }
    service.ajaxEnd=function(){
        service.ajaxCallCount--;
        if(0===service.ajaxCallCount){
            setTimeout(function(){
                if(service.onAfterAjax)service.onAfterAjax();
            },200);

        }
    }
    service.showErrors=function($scope,fail){
        if(!$scope)return;
        var errors=new Array();
        errors.main=fail.main;
       /* angular.forEach(fail.failures,function(p,i){
            var current=service.find(errors,function(m){ return m.key===pp.key;});
            if(current){
               // current.text=current.text+
            }
        });*/
    }
    service.clearErrors=function($scope){
        if(!$scope)return;
        $scope.$$errors=new Array();
        $scope.$$errors.main="";
    }
  return service;
}]);
