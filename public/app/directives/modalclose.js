/**
 * Created by sempa on 2/20/2016.
 */
'use strict'
surveyApp.directive('modalClose',function(){
    return{
        restrict:'A',
        link:function(scope,elem,attr){
            scope.dismiss=function(){
                elem.modal('hide');
            }
        }
    }
})
