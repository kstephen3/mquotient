/**
 * Created by sempa on 1/26/2016.
 */
var surveyApp=angular.module('surveyApp',['ngAnimate','ngRoute']);
surveyApp.config(function($routeProvider){
    $routeProvider
        .when('/',{
            templateUrl:'/app/views/home.html',
            controller:'mainController'
        })
        .when('/login',{
            templateUrl:'/app/views/login.html',
            controller:'mainController'
        }).when('/api/signup',{
            templateUrl:'/app/views/signup.html',
            controller:'mainController'
        })
        .when('/api/surveys',{
            templateUrl:'/app/views/survey_list.html',
            controller:'mainController'
        })
        .when('/api/create/question/:id',{
        templateUrl:'/app/views/create_question.html',
        controller:'mainController'
        })
        .when('/api/survey/questions/:id',{
            templateUrl:'/app/views/question_list.html',
            controller:'mainController'
        })
        .when('/api/survey/attempt/questions/:surveyId',{
            templateUrl:'/app/views/attempt_questions.html',
            controller:'attemptQuestionsController'
        })




});







