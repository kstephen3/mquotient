/**
 * Created by sempa on 2/20/2016.
 */
'use strict'
surveyApp.controller('mainController',['$scope','$route','dataService','$location',function($scope,$route,dataService,$location){

    var surveyRow=function(){
        this.name= '';
        this.description= '';
        this.questions= new Array();
    }
    var questionRow=function(){
        this.questionId= '';
        this.question= '';
        this.surveyId= '';
        this.scale=0;
        this.answers= new Array();
    }
    var companyUserCode =function(surveyId){
        this.surveyId=surveyId;
        this.phoneNumber='256758227539'
    }
    $scope.companyUserCodeRow=new companyUserCode();
    $scope.surveyRows=new Array();
    $scope.questionRows=new Array();
    $scope.userCompanyCode='';
    $scope.$on('$routeChangeSuccess',function(){
        dataService.get('api/survey/list').then(function(response){
            if(response.status==='success'){
                console.log(response);
                angular.forEach(response.data,function(data,index){
                    var row=new surveyRow();
                    row.name=data.name;
                    row.description=data.description;
                    row.surveyId= data._id;
                    angular.forEach(data.questions,function(question,index){
                        var qrow=new questionRow();
                        qrow.question=question.question;
                        qrow.questionId=question.surveyId;
                        qrow.surveyId= question._id;
                        qrow.scale=question.scale;
                        row.questions.push(qrow);
                    });
                    $scope.surveyRows.push(row);
                });
            }else{
                //alert('error, '+response.message);
                console.log('error, '+response.message)
            }
        },function(err){
            console.log('error, '+err)
            alert('error, '+err);
        });
    });
    $scope.companyUserCodes=new Array();
    $scope.surveyQuestions=function(surveyId){
        $location.path('/api/survey/questions/'+surveyId);
    }
    $scope.resetDialogData=function(){
        $scope.userCompanyCode='';
        $scope.error=false;
        $scope.errorMessage='';
    }
    $scope.activateUserSurvey=function(){
        dataService.get('/api/user/code/'+$scope.userCompanyCode,$scope).then(function(response){
            if(response.status==='success'){
                $scope.userCompanyCode='';
                $scope.dismiss();
                $location.path('/api/survey/attempt/questions/'+response.data.surveyId);
            }else{
                //alert(response.message);
                $scope.error=true;
                $scope.errorMessage=response.message;
            }
        })
    }
    $scope.addUserCodeEntry=function(surveyId){
        $scope.surveyId=surveyId;
        $scope.companyUserCodes.push(new companyUserCode(surveyId))
    }
    $scope.removeCompanyUserCode=function(row){
        var index= $scope.companyUserCodes.indexOf(row);
        $scope.companyUserCodes.splice(index,1);
    }
    $scope.createCompanyUserCodes=function(){
        var codes={
            codes:$scope.companyUserCodes
        }


        dataService.post('/api/survey/create/user/codes/'+$scope.surveyId,codes,$scope).then(function(response){
            if(response.status==='success'){
                $scope.dismiss();
                $scope.companyUserCodes.splice(0);
                alert(response.message);
            }else{
                alert(response.message);
            }
        },function(error){
            if(error){
                alert(error)
            }
        })
    }
}]);
