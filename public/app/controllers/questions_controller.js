/**
 * Created by sempa on 2/20/2016.
 */
'use strict'
surveyApp.controller('questionsController',['$scope','$route','dataService','$location',function($scope,$route,dataService,$location){
    var surveyQuestion=function(surveyId){
        this.surveyId=surveyId;
        this.question='';
        this.scale=0;
        this.isForScale=false;
        this.isYesNo=false;
        this.isTextOnly=false;
        this.answers=new Array();
    }
    $scope.objQuestion=new surveyQuestion($scope.surveyId);

    //$scope.questionRows=new Array();
    $scope.answerRows=new Array();
    var questionRow=function(surveyId){
        this.questionId= '';
        this.question= '';
        this.surveyId= $scope.surveyId;
        this.scale=0;
        this.isForScale=false;
        this.isYesNo=false;
        this.isTextOnly=false;
        this.answers= new Array();
    }
    var answerRow=function(){
        this.answerId='';
        this.answer='';
        this.questionId='';
        this.scale=0;
    }
    $scope.cancelQuestionAdd=function(){
        $scope.objQuestion=new surveyQuestion($scope.surveyId);
    }
    $scope.removeAnswer=function(row){
        var index=$scope.objQuestion.answers.indexOf(row);
        $scope.objQuestion.answers.splice(index,1);
    }
    $scope.addNewAnswer=function(){
        //$scope.answerRows.push(new  $scope.answerRow());
        $scope.objQuestion.answers.push(new answerRow());
    }
    $scope.saveQuestion=function(){

        if($scope.objQuestion.question.length==0){
            alert("Question can not be empty")
            return;
        }
        if($scope.objQuestion.isForScale===false&&$scope.objQuestion.isYesNo===false&&$scope.objQuestion.isTextOnly===false){
            alert("Which type of question is it? " +
                " Please tick one of the check boxes located next to the question input" +
                "And try again")
            return;
        }
        if($scope.objQuestion.isForScale===true && $scope.objQuestion.scale<=1 ){
            alert("The question scale must be greater than one or else " +
                " Please untick the check box that has \"With Scale?\"located next to the question input" +
                "And try again")
            return;
        }
        if($scope.objQuestion.isForScale===false && $scope.objQuestion.scale>1){
            $scope.objQuestion.scale=1;
        }
        dataService.post('api/survey/create/question/'+$scope.surveyId,$scope.objQuestion,$scope).then(function(response){
            if(response.status==='success'){
                console.log(response);
                //$location.path('/api/survey/questions/'+response.data.surveyId);
                $scope.cancelQuestionAdd();
                $scope.dismiss();
                seedQuestionData(response.data)

            }else{
                //alert('error, '+response.message);
                console.log('error, '+response.message)
            }
        },function(err){
            console.log('error, '+err)
            alert('error, '+err);
        });
    }
    $scope.deleteQuestion=function(row){
        dataService.delete('api/survey/delete/question/'+row.questionId,$scope).then(function(response){
            if(response.status==='success'){
                console.log(response);
                var index=$scope.questionRows.indexOf(row);
                $scope.questionRows.splice(index,1);



            }else{
                //alert('error, '+response.message);
                console.log('error, '+response.message)
            }
        },function(err){
            console.log('error, '+err)
            alert('error, '+err);
        });
    }
    $scope.$on('$routeChangeSuccess',getSurveyQuestions);
    function getSurveyQuestions(){

        console.log($route.current.params)
        $scope.surveyId=$route.current.params.id
        dataService.get('/api/survey/questions/'+$route.current.params.id).then(function(response){

            if(response.status==='success'){
                $scope.questionRows=new Array();
                console.log(response);
                seedQuestionData(response.data);
            }else{
                //alert('error, '+response.message);
                console.log('error, '+response.message)
            }
        },function(err){
            console.log('error, '+err)
            alert('error, '+err);
        });
    }
    function seedQuestionData(data){
        angular.forEach(data,function(qn,index){
            console.log(qn);
            var qrow=new questionRow(qn.surveyId);
            qrow.question=qn.question;
            qrow.surveyId=qn.surveyId;
            qrow.questionId= qn._id;
            qrow.scale=qn.scale;
            qrow.isForScale=qn.isForScale;
            qrow.isYesNo=qn.isYesNo;
            qrow.isTextOnly=qn.isTextOnly;
            var qnScale=qrow.scale
            angular.forEach(qn.answers,function(an,index){
                console.log(an);
                var anRow=new answerRow();
                anRow.answerId=an._id;
                anRow.answer=an.answer;
                anRow.scale=parseInt(qnScale)//qn.scale;
                anRow.questionId= an.questionId;
                qrow.answers.push(anRow);
            });
            $scope.questionRows.push(qrow);

        });
    }
}]);
