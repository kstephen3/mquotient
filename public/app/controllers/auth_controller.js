/**
 * Created by sempa on 2/20/2016.
 */
'use strict'
surveyApp.controller('authController',['$scope','dataService','$location',function($scope,dataService,$location){
    $scope.userCredentials={
        password:'',
        userName:''
    }
    var newUser=function(){
        this.firstName='';
        this.lastName='';
        this.phone='';
        this.email='';
        this.userName='';
        this.password='';
        this.confirmPassword='';
    }
    $scope.user=new newUser();
    $scope.register=function(){
        dataService.post('/api/survey/register',$scope.user,$scope).then(function(response){
            if(response.status==='success'){
                $location.path('/');
            }
        },function(error){
            alert('Error registering user')
            console.log('Error registering user '+error);
        });
    }

    $scope.getLogin=function(){
        $location.path('/api/login')
    }
    $scope.logout=function(){
        dataService.get('/api/logout').then(function(response){
            if(response.status=='success'){
                authenticate(false);
                $location.path('/api/login');
            }else{
                alert('error logging out');
            }
        },function(error){
            alert('error logging out')
        });
    }
    $scope.login=function(isValid){

        $scope.isSubmited=true;
        if(isValid){
            dataService.post('/api/login',$scope.userCredentials).then(function(response){
                if(response.status==='success'&&response.message==='authenticated'){
                    authenticate(true);
                    $scope.userCredentials.userName=null;
                    $scope.userCredentials.password=null;
                    console.log('userData: '+response.data)
                    $location.path('/api/surveys')
                }else{
                    alert('error, '+response.message);
                    $scope.userCredentials.password=null;
                    console.log('error, '+response.message)
                }
            },function(err){
                console.log('error, '+err)
                alert('error, '+err);
            })

        }


    }
    function authenticate(flag){
        $scope.authenticated=flag;
    }
}])
