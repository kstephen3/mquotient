/**
 * Created by sempa on 1/3/2016.
 */
var obj=function(){
    this.status=null;
    this.message=null;
    this.data=null;
    this.error=null;
}
exports.jsonResBuilder=function(status,message,data,err){
    var res
    var resData= new obj();
    resData.status=status;
    resData.message=message;
    resData.data=data;
    resData.error=err;
    return resData;
}
