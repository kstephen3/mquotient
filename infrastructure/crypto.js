var bCrypt = require('bcrypt-nodejs');
exports.encryptPassword = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}
exports.isValidUserPassword = function (clientPassword, dbPassword) {
    return bCrypt.compareSync(clientPassword, dbPassword);
}