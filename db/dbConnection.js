/**
 * Created by sempa on 1/24/2016.
 */
'use strict'

var mongoose = require('mongoose');
var uriUtil = require('mongodb-uri');
var hostDbUrl;
exports.startConnection=function(){

    /*
     * Mongoose by default sets the auto_reconnect option to true.
     * We recommend setting socket options at both the server and replica set level.
     * We recommend a 30 second connection timeout because it allows for
     * plenty of time in most operating environments.
     */
    var dbName="SurveyDb";
    var options={server:{socketOptions:{keepAlive:1,connectTimeoutMS:30000}},
        replset:{socketOptions:{keepAlive:1,connectTimeoutMS:30000}}};
    var remoteMongodbUri = null;
    var formatedRemoteMongodbUri = null
    var localMongodbUri=null

    if(typeof process.env.OPENSHIFT_MONGODB_DB_URL==="undefined"){
        //remoteMongodbUri = 'mongodb://admin:andromeda@ds061474.mongolab.com:61474/smsdbprod';
        remoteMongodbUri = process.env.OPENSHIFT_MONGODB_DB_URL+ process.env.OPENSHIFT_APP_NAME
        //remoteMongodbUri = 'mongodb://admin:TFj6GiDgtw7V@127.2.226.130:27017/apps';
        formatedRemoteMongodbUri = uriUtil.formatMongoose(remoteMongodbUri);
        localMongodbUri='mongodb://127.0.0.1/'+dbName;

        try
        {
            mongoose.connect(localMongodbUri,options);
        }catch(err){
            console.log(err);
        }
        mongoose.connection.on('connected',function(){
            console.log("mongoose locally connected to "+ localMongodbUri);
        });
        mongoose.connection.on('error',function(err){
            console.log("Error connecting to local mongoose at "+ localMongodbUri);
            console.log(err);
        });


    }else{
        // remoteMongodbUri = 'mongodb://admin:TFj6GiDgtw7V@127.2.226.130:27017/apps';
        remoteMongodbUri = process.env.OPENSHIFT_MONGODB_DB_URL+ process.env.OPENSHIFT_APP_NAME;
        formatedRemoteMongodbUri = uriUtil.formatMongoose(remoteMongodbUri);
        localMongodbUri='mongodb://localhost/'+dbName;
        try
        {
            mongoose.connect(formatedRemoteMongodbUri,options);
        }catch(err){
            console.log(err);
        }
        mongoose.connection.on('connected',function(){
            console.log("mongoose connected remotely to "+ formatedRemoteMongodbUri);
        });
        mongoose.connection.on('error',function(err){
            console.log("Error connecting to remote mongoose at "+ formatedRemoteMongodbUri);
            console.log(err);
        });
    }


    mongoose.connection.on('disconnected',function(err){
        console.log("Mongoose disconnected ");
    });
    process.on('SIGINT', function () {
        mongoose.connection.close(function () {
            console.log("mongoose disconnected  through app termination")
            process.exit(0);
        });
    });

    //}

    // return{
    //     connect:this.connect
    //}
}


