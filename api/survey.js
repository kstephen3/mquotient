/**
 * Created by sempa on 1/24/2016.
 */
    'use strict'
var mongoose=require('mongoose'),
    Schema=mongoose.Schema,
    Survey=mongoose.model('Survey'),
    Question=mongoose.model('Question'),
    Answer=mongoose.model('Answer'),
    User=mongoose.model('User'),
    CompanyUserCode=mongoose.model('CompanyUserCode'),
    jsonRes=require('../infrastructure/response'),
    crypto=require('../infrastructure/crypto');
exports.createSurvey=function(req,res){
    res.render('create_survey',{title:'Create Survey',name:'',description:''})
}
exports.deleteSurvey=function(req,res){
    Survey.remove({_id:req.params.surveyId},function(err,survey){
    if(err) {
        console.log('Failure in deleting the survey with id: '+req.param.surveyId);
        return res.send('An error occured while deleting the survey with id :' + req.params.surveyId);
    }else{
        if(survey){
            console.log("Success in deleting the survey"+ req.params.surveyId);
          return  res.redirect('/survey/list');
        }
    }

})
}
exports.doCreateSurvey=function(req,res){
    var body=req.body,
        surveyName=body.name,
        description=body.description;
    if(!surveyName){
        //res.send('A survey must a a name to identify it');
      return res.render('create_survey',{title:'Create Survey',name:surveyName,description:description});
    }

    var newSurvey=new Survey({
        name:surveyName,
        description:description
    }).save(function(err,survey){
            if(err){
                console.log('Error saving the survey with : '+err);
                res.send('Error saving the survey with name : '+surveyName);
            }
            if(survey){
                console.log('Success saving the survey '+ survey);
                res.redirect('/survey/list')
            }
        })

}
exports.doCreateQuestion=function(req,res){
    var body=req.body;
    //var question=body.question;
    var clientQn={}
    clientQn.surveyId=req.params.surveyId;;
    clientQn.question=body.question;
    clientQn.scale=body.scale;
    clientQn.isForScale=body.isForScale;
    clientQn.isYesNo=body.isYesNo;
    clientQn.isTextOnly=body.isTextOnly;
    clientQn.answers=body.answers;



    console.log('data from the client: '+JSON.stringify(clientQn));
   //return  res.json(jsonRes.jsonResBuilder('success','success',question,''));
    var scale=body.questionScale
        ,surveyId=req.params.surveyId;;
    var newQuestion=new Question({
        question: clientQn.question,
        scale:clientQn.scale,
        surveyId:clientQn.surveyId,
        isForScale:clientQn.isForScale,
        isYesNo:clientQn.isYesNo,
        isTextOnly:clientQn.isTextOnly
    });
    var answers=new Array();
    clientQn.answers.forEach(function(item,index){
        answers.push(new Answer({answer:item.answer}));
    })

    if(clientQn.question){
       newQuestion.save(function(err,question){
            if(err){
                console.log('Error saving the question with : '+err);
                //return  res.render('create_question',{title:'PQuotient-Create Questionaire',questionObj:question});
                res.json(jsonRes.jsonResBuilder('error','Error saving the question with '+surveyId,'',err));
            }
            if(question){
                answers.forEach(function(answer,index){
                    console.log('looped answer : '+answer);
                    answer.questionId=question;
                    answer.save(function(err,answer){
                        if(err){
                            console.log('Error saving the answer with : '+err);
                        }
                        if(answer){
                            console.log('Success saving the answer with : '+answer);
                            question.answers.push(answer);
                            question.save(function(err,qWithAnswers){
                                if(err){
                                    console.log('Error saving the questions with answers: '+err);
                                }
                                if(qWithAnswers){
                                    console.log('Success saving the questions with answers: '+qWithAnswers);
                                    /*Survey.findOne({_id:surveyId},function(err,survey){
                                     if(err){
                                     console.log('Error finding the survey to seed the question: '+err);
                                     }
                                     if(survey){
                                     survey.questions.push(qWithAnswers)
                                     survey.save(function(serr,survey){
                                     if(survey){
                                     console.log('Success saving the survey with question: '+qWithAnswers);
                                     return res.redirect('/survey/questions/'+surveyId)
                                     }
                                     if(serr){
                                     console.log('Error saving the survey with question: '+err);
                                     return res.redirect('/survey/questions/'+surveyId)
                                     }
                                     })
                                     }
                                     })*/
                                }
                            });
                        }
                    })
                });
                console.log('Success saving the question '+ question);
                Survey.findOne({_id:surveyId},function(err,survey){
                    if(err){
                        console.log("failed finding the survey with the id: "+surveyId);
                    }
                    if(survey){
                        survey.questions.push(question)
                        survey.save(function(err,updatedSurvey){
                            if(err){
                                console.log("Error updating the survey with the question :"+surveyId);

                            }
                            if(updatedSurvey){
                                //return res.redirect('/survey/questions/'+surveyId)
                                Question.find({_id:question._id}).populate({
                                    path:'answers'
                                }).exec(function(err,_question){
                                    if(err){
                                        console.log('Error finding the created question,'+ err);
                                        //res.send('Error finding any questions,please create some');
                                    }
                                    if(_question){
                                        //res.render('question_list',{title:'questions',questions:questions});
                                        //.log('success finding any questions,please create some '+ questions);
                                       // res.render('user_servey',{title:'questions',questions:questions});
                                        console.log("Success updating the survey with the question and retrieving the qn:"+_question);
                                        return  res.json(jsonRes.jsonResBuilder('success','question successfully saved',_question,''));
                                    }
                                })


                            }
                        })
                    }
                })
                //res.redirect('/questions/list')
                //res.redirect('/question/list/'+surveyId)
            }
        });/* */
    }else{
        //res.render('create_question',{title:'create question',questionObj:newQuestion});
        return  res.json(jsonRes.jsonResBuilder('fail','question can not be empty','',''));

    }
}
exports.doDeleteQuestion=function(req,res){
    var qnId=req.params.questionId;
    Answer.remove({questionId:qnId},function(err,answer){
        if(err){
            console.log('Error deleting answers for question with id :'+qnId);
           return res.json(jsonRes.jsonResBuilder('error','Error deleting answers for question with id :'+qnId,'',err));
        }if(answer){
            console.log('success deleting the answer'+answer);
            Question.remove({_id:qnId},function(err,question){
                if(err){
                    console.log('Error finding  question with id :'+qnId);
                    return res.json(jsonRes.jsonResBuilder('error','Error deleting answers for question with id :'+qnId,'',err));
                }if(question){
                    console.log('success deleting the question'+question);
                 return   res.json(jsonRes.jsonResBuilder('success','success deleting the question','',''));
                    /*Survey.remove({questionId:qnId},function(err,survey){
                        if(err){
                            console.log('Error deleting  Survey with questionId :'+qnId);
                            res.json(jsonRes.jsonResBuilder('error','Error deleting  Survey with questionId :'+qnId,'',err));
                        }if(survey){
                            res.json(jsonRes.jsonResBuilder('success','success deleting the question','',''));
                        }

                    })*/

                }
            })
        }
       //return res.json(jsonRes.jsonResBuilder('fail','failed to delete the question','',''));
    })

}
exports.createQuestion=function(req,res){
    var surveyId=req.params.surveyId;
    if(surveyId){
        Survey.findOne({_id:surveyId},function(err,survey){
            if(err){
                console.log('Error finding  survey with id :'+surveyId);
                res.json(jsonRes.jsonResBuilder('error','Error finding  survey with id'+surveyId,'',err));
            }
            if(survey){
                console.log('Success finding  survey :'+survey);
                 res.render('create_question',{title:'create question',questionObj:new Question({
                    surveyId:survey._id,
                    question: '',
                    scale:'',})});

                res.json(jsonRes.jsonResBuilder('success','success',surveys,''));
            }
        })
    }else{
        console.log('serveyId is null');
        res.redirect('create_question',{title:'create question',questionObj:new Question()});
    }

}
exports.surveyList=function(req,res){
    Survey.find({}).populate({
        path:'questions',
        populate:{path:'answers'}
    }).exec(function(err,surveys){
        if(err){
            console.log('Error finding any surveys,please create some '+ err);
            //res.send('Error finding any surveys,please create some');
            res.json(jsonRes.jsonResBuilder('error','Error finding any surveys,please create some','',err));
        }
        if(surveys){
            //res.render('survey_list',{title:'surveys',surveys:surveys});
            console.log('Success finding  surveys'+ surveys);
            res.json(jsonRes.jsonResBuilder('success','success',surveys,''));
        }
    })
}
exports.questionList=function(req,res){
    Question.find({}).populate({
        path:'answers'
    }).exec(function(err,questions){
        if(err){
            console.log('Error finding any questions,please create some '+ err);
            res.send('Error finding any questions,please create some');
        }
        if(questions){
            //res.render('question_list',{title:'questions',questions:questions});
            console.log('success finding any questions,please create some '+ questions);
            res.render('user_servey',{title:'questions',questions:questions});
        }
    })
}
exports.questionListBySurveyId=function(req,res){
    var surveyId=req.params.surveyId;
    Question.find({surveyId:surveyId}).populate('answers').exec(function(err,questions){
        if(err){
            console.log('Error finding  questions for survey with id'+surveyId+' '+'please create some '+ err);
            //return res.send('Error finding any questions,please create some');
            res.json(jsonRes.jsonResBuilder('error','Error finding any questions for survey with id'+surveyId+' '+'please create some ','',err));
        }
        if(questions){
            //res.render('question_list',{title:'questions',questions:questions});
            console.log('success finding  questions '+ questions);
            //res.render('question_list',{title:'questions',surveyId:surveyId,questions:questions});
            res.json(jsonRes.jsonResBuilder('success','success',questions,''));
        }
    })
}



exports.doRegisterUser = function (req,res) {
    var body=req.body;
    var  username=body.userName, password=body.password;

    User.findOne({ userName: username }, function (err, user) {
        if (err) {
            console.log('Error finding user: '+err);
            res.json(jsonRes.jsonResBuilder('error','An error has occured','',''));
        }
        if (user) {
            console.log('user with username: ' + username + ' already exists');
            res.json(jsonRes.jsonResBuilder('exists','user with username: ' + username + ' already exists','',''));
        } else {
            var newUser = new User();
            newUser.userName= username;
            newUser.password= crypto.encryptPassword(password);
            newUser.email= body['email'];
            newUser.firstName= body.firstName;
            newUser.lastName= body.lastName;
            newUser.save(function (err, _newUser) {
                if (err) {
                    console.log('Error in saving user: '+err);
                    return res.json(jsonRes.jsonResBuilder('error','error creating user','',''));
                    //throw err;
                }
                if(_newUser){
                    console.log('Success in saving user: '+_newUser);
                    return res.json(jsonRes.jsonResBuilder('success','saved',{contactPersonId:_newUser._id},''));
                }
                console.log('failed in saving user: ');
                res.json(jsonRes.jsonResBuilder('fail','not saved','',''));
            });

        }

    });
}
exports.doLogOut = function (req, res) {
    req.session.destroy(function () {
        res.locals.isAuthenticated = false;
        // console.log("Session Data: "+req.session.userName);
        return res.json(jsonRes.jsonResBuilder('success','logged out','',''));
    });
    // return res.json(jsonRes.jsonResBuilder('success','logged out','',''));
}

exports.login=function(req,res){
    var username= req.body.userName;
    var password= req.body.password;
    if(!(username&&password)){
        console.log("request body: "+JSON.stringify(req.body));
        res.send("invalid user name and or password ")
        return  res.json(jsonRes.jsonResBuilder('error','invalid user name and or password','',''));
    }
    User.findOne({ userName: username }, function (err, user) {

        if (err) {
            return done(err)
        }
        if (!user) {
            console.log('user with username' + username + ' not found');
            return  res.json(jsonRes.jsonResBuilder('fail','user with username' + username + ' not found','',''));
        }
        if (!crypto.isValidUserPassword( password,user.password)) {
            return res.json(jsonRes.jsonResBuilder('fail','invalid password','',''));
        }
        res.locals.isAuthenticated = true;
        req.session.isAuthenticated = true;
        req.session.user=user;
        console.log('authenticated');
        return  res.json(jsonRes.jsonResBuilder('success','authenticated',user.userName,''));
    })
}
exports.getAuth=function(req,res){
    if(req.session.isAuthenticated){
        console.log('authenticated');
        res.json(jsonRes.jsonResBuilder('authenticated','authenticated','',''));
    }else{
        console.log('user not logged in');
        res.json(jsonRes.jsonResBuilder('unauthenticated','user not logged in ','',''));
    }
}
exports.getUserCode=function(req,res){
   // var body=req.params.phoneNumber;
    //var surveyId=body.surveyId,
    //console.log('body '+ JSON.stringify(req.body));
     //var phone=crypto.encryptPassword(req.params.phoneNumber);
    var phone=req.params.phoneNumber;
    console.log('phone '+ phone);
    CompanyUserCode.findOne({code:phone},function(err,code){
        if(err){
            console.log('An error occurred while finding your code for '+phone+ ' ,try again or contact your company administrator')
           return res.json(jsonRes.jsonResBuilder('error','An error occurred while finding your code,try again or contact your company administrator','',err));
        }
        if(code){
            console.log('success retrieving the user code for '+phone)
          return  res.json(jsonRes.jsonResBuilder('success','success retrieving the user code',code,''));
        }else{
            console.log('failed retrieving the user code for '+phone+ ' ,probably it does\'nt exist,contact your systems admin')
            return  res.json(jsonRes.jsonResBuilder('fail','failed retrieving the user code,probably it does\'nt exist,contact your systems admin','',''));
        }
        // if we have reached this far,something is wrong

    })
}

exports.doCreateCompanyUserCodes=function(req,res){
    var body=req.body;
    var surveyId=req.params.surveyId,
        arrPhones=body.codes;
    console.log('codes from client '+arrPhones)//JSON.stringify( body));
    if(!surveyId){
      return res.json(jsonRes.jsonResBuilder('fail','survey id can not be null','',''))
    }

    arrPhones.forEach(function(phone,index){
       console.log('Code from client '+ phone.phoneNumber);
        //var encPhoneNo=crypto.encryptPassword(phone.phoneNumber)
        var encPhoneNo=phone.phoneNumber
       CompanyUserCode.findOne({/*surveyId:surveyId,*/code:encPhoneNo},function(err,code){
           console.log('hit method');
           if(err){
           console.log('An error occurred while finding your code,try again or contact your company administrator');
           }
           if(code){
               //res.redirect('/survey/questions/'+surveyId)
               console.log('User code already exists,skipping save operation.......');

           }else{
               console.log('hit else');
               Survey.findOne({_id:surveyId},function(err,survey){
                   if(err){
                       console.log('An error has occurred while finding the survey '+err);
                   }
                   if(survey){
                       console.log('Success in finding  survey ');
                       var userCode=new CompanyUserCode({
                           code:encPhoneNo,
                           surveyId:survey});

                       userCode.save(function(err,companyUserCode){
                           if(err){
                               console.log('An error has occurred while saving the companyUserCode '+err);
                           }if(companyUserCode){
                               console.log('Success saving the companyUserCode '+companyUserCode);
                           }
                       })
                   }
               })

           }
       })
   })
    res.json(jsonRes.jsonResBuilder('success','Success creating user codes for the above company users','',''));
}