/**
 * Created by sempa on 1/24/2016.
 */
    'use strict'
var mongoose=require('mongoose'),
    Schema=mongoose.Schema,
    Survey=mongoose.model('Survey'),
    Question=mongoose.model('Question'),
    Answer=mongoose.model('Answer');
exports.createSurvey=function(req,res){
    res.render('create_survey',{title:'Create Survey',name:'',description:''})
}
exports.deleteSurvey=function(req,res){
    Survey.remove({_id:req.params.surveyId},function(err,survey){
    if(err) {
        console.log('Failure in deleting the survey with id: '+req.param.surveyId);
        return res.send('An error occured while deleting the survey with id :' + req.params.surveyId);
    }else{
        if(survey){
            console.log("Success in deleting the survey"+ req.params.surveyId);
          return  res.redirect('/survey/list');
        }
    }

})
}
exports.doCreateSurvey=function(req,res){
    var body=req.body,
        surveyName=body.name,
        description=body.description;
    if(!surveyName){
        //res.send('A survey must a a name to identify it');
      return res.render('create_survey',{title:'Create Survey',name:surveyName,description:description});
    }

    var newSurvey=new Survey({
        name:surveyName,
        description:description
    }).save(function(err,survey){
            if(err){
                console.log('Error saving the survey with : '+err);
                res.send('Error saving the survey with name : '+surveyName);
            }
            if(survey){
                console.log('Success saving the survey '+ survey);
                res.redirect('/survey/list')
            }
        })

}
exports.doCreateQuestion=function(req,res){
var body=req.body;
    var question=body.question;
    var scale=body.questionScale
        ,surveyId=body.surveyId;
    var newQuestion=new Question({
        question: question,
        scale:scale,
        surveyId:surveyId
    });
    var answers=new Array();
    var answerOne=body.answerOne;
    var answerTwo=body.answerTwo;
    var answerThree=body.answerThree;
    var answerFour=body.answerFour;
    if(answerOne){
        answers.push(new Answer({answer:answerOne}))
    }
    if(answerTwo){
        answers.push(new Answer({answer:answerTwo}))
    }
    if(answerThree){
        answers.push(new Answer({answer:answerThree}))
    }
    if(answerFour){
        answers.push(new Answer({answer:answerFour}))
    }

    if(question){
       newQuestion.save(function(err,question){
               if(err){
                   console.log('Error saving the question with : '+err);
                   return  res.render('create_question',{title:'PQuotient-Create Questionaire',questionObj:question});
               }
               if(question){

                   answers.forEach(function(answer,index){
                       console.log('looped answer : '+answer);
                       answer.questionId=question;
                       answer.save(function(err,answer){
                            if(err){
                                console.log('Error saving the answer with : '+err);
                            }
                            if(answer){
                                console.log('Success saving the answer with : '+answer);
                                question.answers.push(answer);
                                question.save(function(err,qWithAnswers){
                                   if(err){
                                       console.log('Error saving the questions with answers: '+err);
                                   }
                                    if(qWithAnswers){
                                        console.log('Success saving the questions with answers: '+qWithAnswers);
                                        /*Survey.findOne({_id:surveyId},function(err,survey){
                                            if(err){
                                                console.log('Error finding the survey to seed the question: '+err);
                                            }
                                            if(survey){
                                                survey.questions.push(qWithAnswers)
                                                survey.save(function(serr,survey){
                                                    if(survey){
                                                        console.log('Success saving the survey with question: '+qWithAnswers);
                                                        return res.redirect('/survey/questions/'+surveyId)
                                                    }
                                                    if(serr){
                                                        console.log('Error saving the survey with question: '+err);
                                                        return res.redirect('/survey/questions/'+surveyId)
                                                    }
                                                })
                                            }
                                        })*/
                                    }
                                });
                            }
                        })
                    });





                   console.log('Success saving the question '+ question);
                   Survey.findOne({_id:surveyId},function(err,survey){
                       if(err){
                           console.log("failed finding the survey with the id: "+surveyId);
                       }
                       if(survey){
                           survey.questions.push(question)
                           survey.save(function(err,updatedSurvey){
                               if(err){
                                   console.log("Error updating the survey with the question :"+surveyId);

                               }
                               if(updatedSurvey){
                                   return res.redirect('/survey/questions/'+surveyId)
                               }
                           })
                       }
                   })
                   //res.redirect('/questions/list')
                   //res.redirect('/question/list/'+surveyId)
               }
           });
   }else{
         res.render('create_question',{title:'create question',questionObj:newQuestion});
   }
}
exports.createQuestion=function(req,res){
    var surveyId=req.params.surveyId;
    if(surveyId){
        Survey.findOne({_id:surveyId},function(err,survey){
            if(err){
                console.log('Error finding  survey with id :'+surveyId);
            }
            if(survey){
                console.log('Success finding  survey :'+survey);
                 res.render('create_question',{title:'PQuotient-Create Questionaire',questionObj:new Question({
                    surveyId:survey._id,
                    question: '',
                    scale:'',})});
            }
        })
    }else{
        console.log('serveyId is null');
        res.redirect('create_question',{title:'PQuotient-Create Questionaire',questionObj:new Question()});
    }

}
exports.surveyList=function(req,res){
    Survey.find({},function(err,surveys){
        if(err){
            console.log('Error finding any surveys,please create some '+ err);
            res.send('Error finding any surveys,please create some');
        }
        if(surveys){
            res.render('survey_list',{title:'PQuotient-Surveys',surveys:surveys});
        }
    })
}
exports.questionList=function(req,res){
    Question.find({}).populate('answers').exec(function(err,questions){
        if(err){
            console.log('Error finding any questions,please create some '+ err);
            res.send('Error finding any questions,please create some');
        }
        if(questions){
            //res.render('question_list',{title:'questions',questions:questions});
            console.log('success finding any questions: '+ questions);
            res.render('question_list',{title:'PQuotient-Questionaire',questions:questions});
        }
    })
}
exports.questionListBySurveyId=function(req,res){
    var surveyId=req.params.surveyId;
    Question.find({surveyId:surveyId}).populate('answers').exec(function(err,questions){
        if(err){
            console.log('Error finding any questions for survey with id'+surveyId+' '+'please create some '+ err);
           return res.send('Error finding any questions,please create some');
        }
        if(questions){
            //res.render('question_list',{title:'questions',questions:questions});
            console.log('success finding any questions '+ questions);
            res.render('question_list',{title:'PQuotient-Questionaire',surveyId:surveyId,questions:questions});
        }
    })
}