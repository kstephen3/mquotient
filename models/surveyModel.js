/**
 * Created by sempa on 1/24/2016.
 */
'use strict'
var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var surveySchema=new Schema({
    name:{type:String,required:true},
    description:{type:String,required:false},
    questions:[{type:Schema.ObjectId,ref:'Question'}]
});

var questionSchema=new Schema({
question:{type:String,required:true},
answers:[{type:Schema.ObjectId,ref:'Answer'}],
surveyId:{type:Schema.ObjectId,ref:'Survey'},
scale:{type:Number},
questionType:{type:String,enum:['YesNo','Scaled']},
isForScale:Boolean,
isYesNo:Boolean,
isTextOnly:Boolean
});

var answerSchema=new Schema({
    answer:{type:String,required:true},
    scale:{type:Number},
    acceptance:{type:Boolean,required:false},//yes or no
    narrative:String,//text narrative answer
    questionId:[{type:Schema.ObjectId,ref:'Question'}]
});

var user = mongoose.Schema({
    firstName: String,
    lastName: String,
    userName: {type:String,unique:true},
    password: String,
    email: String,
    phone: String,
    address: String,
});
var companyUserCodeSchema=mongoose.Schema({
    code:{type:String},//salt of the phone number provided by the company
    surveyId:{type:Schema.ObjectId,ref:'Survey'}
})
var surveyOperationSchema=mongoose.Schema({
    userAttemptedSurveys:[{type:Schema.ObjectId,ref:'CompanyUserCode'}],
    attemptedQuestions:{type:Schema.ObjectId,ref:'Question'}
})
mongoose.model('User', user);
mongoose.model('Survey',surveySchema);
mongoose.model('Question',questionSchema);
mongoose.model('Answer',answerSchema);
mongoose.model('CompanyUserCode',companyUserCodeSchema);
mongoose.model('SurveyOperation',surveyOperationSchema);